/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COMPONENT_FACTORY_PRELOADER__COMPONENT_FACTORY_PRELOADER_DICT_H
#define COMPONENT_FACTORY_PRELOADER__COMPONENT_FACTORY_PRELOADER_DICT_H

#include <ComponentFactoryPreloader/ComponentFactoryPreloader.h>

#endif
