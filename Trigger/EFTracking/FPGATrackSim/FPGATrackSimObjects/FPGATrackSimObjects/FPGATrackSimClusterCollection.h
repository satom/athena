/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FPGATRACKSIMCLUSTERCOLLECTION_H
#define FPGATRACKSIMCLUSTERCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "AthenaKernel/CLASS_DEF.h"
#include "FPGATrackSimObjects/FPGATrackSimCluster.h"


typedef std::vector<FPGATrackSimCluster> FPGATrackSimClusterCollection;
CLASS_DEF( FPGATrackSimClusterCollection , 1277108463 , 1 )



#endif // FPGATRACKSIMCLUSTERCOLLECTION_DEF_H
